export const convertToCM = (value: string) => parseFloat(value) * 100;
