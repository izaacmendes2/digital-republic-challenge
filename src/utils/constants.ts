export const ALTURAPORTA = 190; //ALTURA EM CM
export const LARGURAPORTA = 80; //LARGURA EM CM
export const LARGURAJANELA = 200; //LARGURA EM CM
export const ALTURAJANELA = 120; //ALTURA EM CM

export const areaTotalExcedida =
  'A área total da parede não deve exceder 50 metros quadrados';
export const areaDaJanelaSuperiora50 =
  'O total de área das portas e janelas ultrapassa o limite permitido, remova alguma porta e/ou janela ou aumente o tamanho da parede';
export const paredebaixa =
  'A altura da sua parede precisa ser pelo menos 30cm maior do que a sua porta';
