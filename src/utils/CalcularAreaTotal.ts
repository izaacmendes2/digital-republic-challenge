import {
  ALTURAJANELA,
  ALTURAPORTA,
  LARGURAJANELA,
  LARGURAPORTA,
} from './constants';

export const calcularAreaTotal = (
  qtdPorta: number = 0,
  qtdJanela: number = 0
) => {
  const areaPorta = qtdPorta * (ALTURAPORTA * LARGURAPORTA);
  const areaJanela = qtdJanela * (ALTURAJANELA * LARGURAJANELA);
  return areaJanela + areaPorta;
};
