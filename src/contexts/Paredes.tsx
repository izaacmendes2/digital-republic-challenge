import React, {
  Dispatch,
  ReactNode,
  SetStateAction,
  createContext,
  useContext,
  useState,
} from 'react';
import { convertToCM } from '../utils/ConvertToCM';

// import { Container } from './styles';
export interface Parede {
  id: string;
  largura: number;
  altura: number;
  qtdPortas: number;
  qtdJanelas: number;
  areaDaParede: number;
  areaJanelaPorta: number;
}
interface ContextProps {
  paredes: Parede[];
  setParedes: Dispatch<SetStateAction<Parede[]>>;
  hasError: boolean;
  setHasError: (_: boolean) => void;
}
const paredesContext = createContext<ContextProps>({});
const ParedesProvider: React.FC<{ children: ReactNode }> = ({ children }) => {
  const [paredes, setParedes] = useState<Parede[]>([
    {
      id: crypto.randomUUID(),
      altura: 1,
      largura: 1,
      qtdJanelas: 0,
      qtdPortas: 0,
      areaDaParede: convertToCM('1') * convertToCM('1'),
      areaJanelaPorta: 0,
    },
  ]);

  const [hasError, setHasError] = useState(false);
  return (
    <paredesContext.Provider
      value={{ paredes, setParedes, hasError, setHasError }}
    >
      {children}
    </paredesContext.Provider>
  );
};
export const useParedes = () => useContext(paredesContext);

export default ParedesProvider;
