import { FormEvent, useState } from 'react';

import './App.css';

import { useParedes } from './contexts/Paredes';

import ParedeGroupFields from './components/paredeGroupFields/ParedeGroupFields';
import { convertToCM } from './utils/ConvertToCM';
import ResultadoModal from './components/ResultadoModal/ResultadoModal';

function App() {
  const { hasError, paredes, setParedes } = useParedes();
  const [showModal, setShowModal] = useState(false);

  const addNewGroupField = () => {
    setParedes((prev) => [
      ...prev,
      {
        id: crypto.randomUUID(),
        altura: 1,
        largura: 1,
        qtdJanelas: 0,
        qtdPortas: 0,
        areaDaParede: convertToCM('1') * convertToCM('1'),
        areaJanelaPorta: 0,
      },
    ]);
  };

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setShowModal(true);
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <fieldset>
          <h1>Calculo de quantidade de tinta</h1>
          <div className="container-inputs">
            {paredes.map((parede) => (
              <ParedeGroupFields {...parede} key={parede.id} />
            ))}
          </div>
          <div className="container-botoes">
            <button
              type="button"
              onClick={addNewGroupField}
              disabled={hasError || paredes.length > 3}
              className="adicionar"
            >
              Adicionar outra parede ao calculo
            </button>
            <button className="calcular" disabled={hasError}>
              Calcular
            </button>
          </div>
        </fieldset>
      </form>
      {showModal && <ResultadoModal setShowModal={setShowModal} />}
    </div>
  );
}

export default App;
