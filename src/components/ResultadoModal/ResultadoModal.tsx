import React, { Dispatch, SetStateAction, useEffect, useState } from 'react';

// import { Container } from './styles';
import './styles.css';
import { useParedes } from '../../contexts/Paredes';
const ResultadoModal: React.FC<{
  setShowModal: Dispatch<SetStateAction<boolean>>;
}> = ({ setShowModal }) => {
  const { paredes } = useParedes();

  let areaDasParedesSomadas = paredes.reduce(
    (soma, current) => soma + current.areaDaParede,
    0
  );
  //Convertendo cm² para m²
  areaDasParedesSomadas /= 10000;
  //Descobrindo qtd de litros
  const litrosDeTinta = areaDasParedesSomadas / 5;
  const latasDe18 = Math.floor(litrosDeTinta / 18);
  let restante = litrosDeTinta % 18;
  //Lata 3.6L
  const latasDe36 = Math.floor(restante / 3.6);
  restante = restante % 3.6;
  //Lata 2.5L
  const latasDe25 = Math.floor(restante / 2.5);
  restante = restante % 2.5;
  //Lata 0.5L
  const latasDe05 = Math.floor(restante / 0.5);

  const totalDeLitros = latasDe05 + latasDe18 + latasDe25 + latasDe36;

  useEffect(() => {
    window.scrollTo({ top: 0 });
    window.document.body.style.overflow = 'hidden';
    return () => {
      window.document.body.style.overflow = 'auto';
    };
  }, []);
  return (
    <div className="containerModal">
      <div className="containerResultado">
        <button className="closeModal" onClick={() => setShowModal(false)}>
          &times;
        </button>
        <div className="containerTexto">
          <p>
            Você vai precisar de {litrosDeTinta} litros de tintas, sendo... {}
          </p>
          <p>
            {latasDe18
              ? `${latasDe18} ${latasDe18 > 1 ? 'latas' : 'lata'} de 2.5L`
              : ''}
          </p>
          <p>
            {latasDe36
              ? `${latasDe36} ${latasDe36 > 1 ? 'latas' : 'lata'} de 3.6L`
              : ''}
          </p>
          <p>
            {latasDe25
              ? `${latasDe25} ${latasDe25 > 1 ? 'latas' : 'lata'} de 2.5L`
              : ''}
          </p>
          <p>
            {totalDeLitros < litrosDeTinta
              ? `${latasDe05 + 1} latas de 0.5`
              : `${latasDe05 > 1 ? 'latas' : 'lata'} de 0.5L`}
          </p>
        </div>
      </div>
    </div>
  );
};

export default ResultadoModal;
