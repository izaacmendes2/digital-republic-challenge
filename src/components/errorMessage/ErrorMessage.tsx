import React from 'react';

// import { Container } from './styles';

const ErrorMessage: React.FC<{ message: string }> = ({ message }) => {
  return <p className="errorMessage">{message}</p>;
};

export default ErrorMessage;
