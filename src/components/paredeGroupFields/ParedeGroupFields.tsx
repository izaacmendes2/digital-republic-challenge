import React, { useEffect, useState } from 'react';
import { calcularAreaTotal } from '../../utils/CalcularAreaTotal';
import {
  ALTURAPORTA,
  areaDaJanelaSuperiora50,
  areaTotalExcedida,
  paredebaixa,
} from '../../utils/constants';
import { convertToCM } from '../../utils/ConvertToCM';
import ErrorMessage from '../errorMessage/ErrorMessage';
import { Parede, useParedes } from '../../contexts/Paredes';

import './styles.css';

const ParedeGroupFields: React.FC<Parede> = ({
  altura,
  areaDaParede,
  areaJanelaPorta,
  largura,
  qtdJanelas,
  qtdPortas,
  id,
}) => {
  const [larguraParede, setLarguraParede] = useState(largura.toString());
  const [alturaParede, setAlturaParede] = useState(altura.toString());
  const [areaParede, setAreaParede] = useState(areaDaParede);

  const [qtdPorta, setQtdPorta] = useState(qtdPortas);
  const [qtdJanela, setQtdJanela] = useState(qtdJanelas);
  const [areaTotalJanelaPorta, setAreaTotalJanelaPorta] =
    useState(areaJanelaPorta);

  const [showErrorMessage, setShowErrorMessage] = useState(false);
  const [ShowMessageParedePequena, setShowMessageParedePequena] =
    useState(false);
  const [ShowMessageParedeBaixa, setShowMessageParedeBaixa] = useState(false);
  const { setHasError } = useParedes();

  const { paredes, setParedes } = useParedes();

  useEffect(() => {
    setAreaTotalJanelaPorta(calcularAreaTotal(qtdPorta, qtdJanela));
    if (qtdPorta) {
      setShowMessageParedeBaixa(
        parseFloat(alturaParede) * 100 - ALTURAPORTA < 30 ? true : false
      );
    }

    setShowMessageParedePequena(
      areaTotalJanelaPorta / areaParede <= 0.5 ? false : true
    );
    const newArray = paredes.map((item) => {
      if (item.id == id) {
        return { ...item, qtdJanelas: qtdJanela, qtdPortas: qtdPorta };
      }
      return item;
    });
    setParedes(newArray as Parede[]);
  }, [qtdPorta, qtdJanela]);

  useEffect(() => {
    setShowMessageParedePequena(
      areaTotalJanelaPorta / areaParede <= 0.5 ? false : true
    );
    const newArray = paredes.map((item) => {
      if (item.id == id) {
        return { ...item, areaDaParede: areaParede };
      }
      return item;
    });
    setParedes(newArray as Parede[]);
  }, [areaParede]);

  useEffect(() => {
    setShowMessageParedePequena(
      areaTotalJanelaPorta / areaParede <= 0.5 ? false : true
    );
    const newArray = paredes.map((item) => {
      if (item.id == id) {
        return { ...item, areaJanelaPorta: areaTotalJanelaPorta };
      }
      return item;
    });
    setParedes(newArray as Parede[]);
  }, [areaTotalJanelaPorta]);

  useEffect(() => {
    if (
      parseFloat(larguraParede) * parseFloat(alturaParede) > 50 &&
      !showErrorMessage
    )
      setShowErrorMessage(true);
    if (parseFloat(larguraParede) * parseFloat(alturaParede) <= 50) {
      setShowErrorMessage(false);

      //area em cm
      setAreaParede(convertToCM(alturaParede) * convertToCM(larguraParede));
    }
    if (qtdPorta) {
      setShowMessageParedeBaixa(
        convertToCM(alturaParede) - ALTURAPORTA < 30 ? true : false
      );
    }
    setShowMessageParedePequena(
      areaTotalJanelaPorta / areaParede <= 0.5 ? false : true
    );
    const newArray = paredes.map((item) => {
      if (item.id == id) {
        return { ...item, altura: alturaParede, largura: larguraParede };
      }
      return item;
    });
    setParedes(newArray as Parede[]);
  }, [larguraParede, alturaParede]);

  useEffect(() => {
    setHasError(
      showErrorMessage || ShowMessageParedeBaixa || ShowMessageParedePequena
    );
  }, [showErrorMessage, ShowMessageParedeBaixa, ShowMessageParedePequena]);
  return (
    <div className="containerGroupField">
      <div className="containerPrincipal">
        <div className="containerLarguraAltura">
          <div className="container-input">
            <label>Largura da parede</label>
            <input
              inputMode="numeric"
              value={larguraParede}
              onChange={({ target }) => {
                if (target.value == '') setLarguraParede('1');
                else setLarguraParede(target.value);
              }}
            />
          </div>
          <div className="container-input">
            <label>Altura da parede</label>
            <input
              value={alturaParede}
              inputMode="numeric"
              onChange={({ target }) => {
                if (target.value == '') setAlturaParede('1');
                else setAlturaParede(target.value);
              }}
            />
          </div>
        </div>

        {showErrorMessage && <ErrorMessage message={areaTotalExcedida} />}
        {ShowMessageParedePequena && (
          <ErrorMessage message={areaDaJanelaSuperiora50} />
        )}
        {ShowMessageParedeBaixa && <ErrorMessage message={paredebaixa} />}
      </div>
      <div className="containerPortaJanela">
        <div className="container-input">
          <label>Quantidade de portas</label>
          <input
            inputMode="numeric"
            type="number"
            onChange={(e) => setQtdPorta(e.target.valueAsNumber)}
          />
        </div>
        <div className="container-input">
          <label>Quantidade de janelas</label>
          <input
            inputMode="numeric"
            type="number"
            onChange={(e) => setQtdJanela(e.target.valueAsNumber)}
          />
        </div>
      </div>
    </div>
  );
};

export default ParedeGroupFields;
