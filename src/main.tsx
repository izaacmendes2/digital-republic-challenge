import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App.tsx';
import './index.css';
import ParedesProvider from './contexts/Paredes.tsx';

ReactDOM.createRoot(document.getElementById('root')!).render(
  <ParedesProvider>
    <App />
  </ParedesProvider>
);
