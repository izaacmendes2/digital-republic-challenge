# 📖 Projeto

O projeto foi desenvolvido usando como base de dados o the movie database. Foi criado todo o fluxo de acesso, desde a home com o faq, a cadastro, autenticação e o próprio consumo da base de dados

## 🧪 Tecnologias

 [![My Skills](https://skillicons.dev/icons?i=html,css,javascript,react)](https://skillicons.dev)

## 🚀 Como executar

Basta clonar o projeto executar **npm i** para instalar as dependecias e depois rodar npm run dev

## ⚠️ Observações

Como a data limite foi muito em cima, não tive tempo de fazer tudo direitinho, mas caso queira olhar outros projetos meus ou fazer uma call pra comentar o código, farei com prazer, por favor não desistam de mim :) meu github https://github.com/zagamendes
